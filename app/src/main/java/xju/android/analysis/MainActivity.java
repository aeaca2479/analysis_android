package xju.android.analysis;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import xju.android.Adapter.MyAdapter;
import xju.android.bean.AnalysisBean;

public class MainActivity extends AppCompatActivity {
    private ListView mListView;
    private EditText mEditText;
    private Button mSubmit;
    private Button mClear;
    private ProgressBar mProgressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListView= (ListView) findViewById(R.id.lv_main);
        mEditText= (EditText) findViewById(R.id.et_content);
        mSubmit= (Button) findViewById(R.id.b_submit);
        mClear= (Button) findViewById(R.id.b_clear);
        mProgressBar= (ProgressBar) findViewById(R.id.pb_wait);
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListView.setAdapter(null);
                String content=mEditText.getText().toString();
                String URL="http://ltpapi.voicecloud.cn/analysis/?api_key=d1a4q7s464u4e6P4B9b4RBGSrLtSyv3qimZMVeJg&text="+content+"。&pattern=all&format=json";
                new MyAsyncTask().execute(URL);
            }
        });
        mClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditText.setText("");
                mListView.setAdapter(null);
            }
        });

    }
    class MyAsyncTask extends AsyncTask<String,Void,List<AnalysisBean>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<AnalysisBean> doInBackground(String... params) {
            return getJsonData(params[0]);
        }

        @Override
        protected void onPostExecute(List<AnalysisBean> analysisBeen) {
            super.onPostExecute(analysisBeen);
            mProgressBar.setVisibility(View.INVISIBLE);
            MyAdapter myAdapter=new MyAdapter(MainActivity.this,analysisBeen);
            mListView.setAdapter(myAdapter);

        }

        private String readStream(InputStream is){
            InputStreamReader isr;
            String result="";
            try {
                String line="";
                isr=new InputStreamReader(is,"utf-8");
                BufferedReader br=new BufferedReader(isr);
                while ((line=br.readLine())!=null){
                    result+=line;
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        private List<AnalysisBean> getJsonData(String url){
            List<AnalysisBean> analysisBeenList=new ArrayList<>();
            try {
                String jsonString=readStream(new URL(url).openStream());
                JSONArray jsonArray=new JSONArray(jsonString).getJSONArray(0).getJSONArray(0);
                JSONObject jsonObject;

                AnalysisBean analysisBean;
                for (int i=0;i<jsonArray.length();i++){
                    jsonObject=jsonArray.getJSONObject(i);
                    analysisBean=new AnalysisBean();
                    analysisBean.setId(jsonObject.getInt("id"));
                    analysisBean.setPos(jsonObject.getString("pos"));
                    analysisBean.setNe(jsonObject.getString("ne"));
                    analysisBean.setCont(jsonObject.getString("cont"));
                    analysisBean.setParent(jsonObject.getInt("parent"));
                    analysisBean.setRelate(jsonObject.getString("relate"));
                    analysisBeenList.add(analysisBean);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return analysisBeenList;
        }
    }
}
