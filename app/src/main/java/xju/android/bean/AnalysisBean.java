package xju.android.bean;

/**
 * Created by aeaca on 2016/9/28.
 */

public class AnalysisBean {
    private int id;//编号
    private String cont;//内容
    private String pos;//词性标注
    private String ne;//命名实体识别
    private int parent;//依存句法分析
    private String relate;//相对应的关系
    private ArgBean arg;//语义角色信息结点，暂无

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCont() {
        return cont;
    }

    public void setCont(String cont) {
        this.cont = cont;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getNe() {
        return ne;
    }

    public void setNe(String ne) {
        this.ne = ne;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public String getRelate() {
        return relate;
    }

    public void setRelate(String relate) {
        this.relate = relate;
    }

    public ArgBean getArg() {
        return arg;
    }

    public void setArg(ArgBean arg) {
        this.arg = arg;
    }
}
