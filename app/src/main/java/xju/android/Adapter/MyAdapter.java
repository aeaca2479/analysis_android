package xju.android.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import xju.android.analysis.R;
import xju.android.bean.AnalysisBean;

/**
 * Created by aeaca on 2016/9/28.
 */

public class MyAdapter extends BaseAdapter{
    private List<AnalysisBean> mList;
    private LayoutInflater mInflater;

    public MyAdapter(Context context,List<AnalysisBean> data){
        mList=data;
        mInflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder=null;
        if(convertView==null){
            viewHolder=new ViewHolder();
            convertView=mInflater.inflate(R.layout.item,null);
            viewHolder.r_fid= (TextView) convertView.findViewById(R.id.r_fid);
            viewHolder.r_pos= (TextView) convertView.findViewById(R.id.r_pos);
            viewHolder.r_ne= (TextView) convertView.findViewById(R.id.r_ne);
            viewHolder.r_cont= (TextView) convertView.findViewById(R.id.r_cont);
            viewHolder.r_parent= (TextView) convertView.findViewById(R.id.r_parent);
            viewHolder.r_relate= (TextView) convertView.findViewById(R.id.r_relate);
            convertView.setTag(viewHolder);
        }else{
            viewHolder= (ViewHolder) convertView.getTag();
        }
        viewHolder.r_fid.setText("编号："+position);
        viewHolder.r_pos.setText("词性："+mList.get(position).getPos());
        viewHolder.r_ne.setText("实体："+mList.get(position).getNe());
        viewHolder.r_cont.setText(mList.get(position).getCont());
        if(mList.get(position).getParent()==-1)
            viewHolder.r_parent.setText("父节点：Root");
        else
            viewHolder.r_parent.setText("父节点："+mList.get(position).getParent());
        viewHolder.r_relate.setText("对应关系："+mList.get(position).getRelate());
        return convertView;
    }

    class ViewHolder{
        public TextView r_fid,r_pos,r_ne,r_cont,r_parent,r_relate;
    }
}
